
rootdir = './photos';
filelist = dir(fullfile(rootdir, '**/*.jpg'));  %get list of files and folders in any subfolder

X = zeros(932 , 700 , 3 , length(filelist) );
%y = strings(length(filelist),1);
y = {};

test = zeros(100, 0);

for k=1:numel(filelist)    
    n = filelist(k).name;
    fold = filelist(k).folder;
    
    full_path = strcat(fold,"/",n);
    class = split(fold,"/");
    class = class(end);
    
    
    X(:, :, :, k) = imread(full_path);
    y(k) = class;
    
    
    
end

save('Xhandphotodata.mat','X', '-v7.3')
save('yhandphotodata.mat', 'y')

%full_path = strcat(path,"/",n);
%img=imread(full_path);
%print (size(img));



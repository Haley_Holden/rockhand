# Hand gesture recognition system
# Computer Vision final project 
# 
# ___________________________________________
# Sarah Bsales and Haley Holden

import cv2
import os
import sys
import numpy as np
from sklearn import svm
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
#from keras.applications.vgg16 import decode_predictions
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt


# layer of the VGG features that will be used
cnn_codes = 'fc2'
clf = None

# an instance of VGG16: we need it to extract features (below)
model = VGG16(weights='imagenet')


# an alternative model, to extract features from the specified layer
# note that we could extract from any VGG16 layer, by name
features_model = Model(inputs=model.input, outputs=model.get_layer(cnn_codes).output)


def classify_svm(img):
    features = extract_vgg_features(img)
    pred = clf.predict(features)

    # show classification result
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img, '{}'.format(pred), (15, 25), font, 1, (0, 255, 0), 2, cv2.LINE_AA)
    return img




def extract_vgg_features(img):
    # prepare the image for VGG
    img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_LINEAR)
    img = img[np.newaxis, :, :, :]
    # call feature extraction
    return features_model.predict(img)



def camera_loop():
    print("Press <SPACE> to capture/classify an image, or <Esc> to exit.")
    cap = cv2.VideoCapture(0)
    while (True):
        _, frame = cap.read()

        action = cv2.waitKey(1)

        if action == ord('q') or action == 27:
            break

            #if action == ord(' '):
            # svm object detection
        frame = classify_svm(frame)
        img_to_show = cv2.resize(frame, (0,0), fx=0.8, fy=0.8)
        cv2.imshow('SVM output:', img_to_show)

    cap.release()



def get_features(img_path):
    #Load image and alter for predictiom

    img = load_img(img_path, target_size=(224, 224))
    input_arr = img_to_array(img)
    input_arr = np.expand_dims(input_arr, axis=0)
    input_arr = preprocess_input(input_arr)
    features = features_model.predict(input_arr)
    return list(features[0])



if __name__ == '__main__':
    
    features = []
    labels = []
    rootdir=  './images'


    for subdir, dirs, files in os.walk(rootdir):
        count = 0
        for f in files:
            if count < 20:
                try:
                    imName = os.path.join(subdir, f)
                    if 'DS_Store' in imName:
                        continue
                    features.append(get_features(imName))
                except:
                    print("invalid file")
                    continue

                label = subdir.split('/')[-1]
                labels.append(label)
                count += 1
                    
   
    features_test = []
    labels_test = []
    rootdir_test =  './Test'

    for subdir, dirs, files in os.walk(rootdir_test):
        count = 0
        for f in files:
            if count < 20:
                try:
                    imName = os.path.join(subdir, f)
                    if 'DS_Store' in imName:
                        continue
                    features_test.append(get_features(imName))
                except:
                    print("invalid file + ", imName)
                    continue
                label = subdir.split('/')[-1]
                print(imName)
                labels_test.append(label)
                count += 1
                    



    X_train, X_val, y_train, y_val = train_test_split(features, labels, test_size=0.2, random_state=27)
    X_test = features_test
    y_test = labels_test

	# The sets below use a section of the original data as the test data subset
    #X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=27)
    #X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=71)



    clf = svm.SVC(kernel='linear').fit(X_train, y_train)


    pred_test = clf.predict(X_test)
    print ("Test Accuracy:" , accuracy_score(y_test, pred_test))   

    pred_train = clf.predict(X_train)
    print("Training Accuracy:", accuracy_score(y_train, pred_train))

    pred_val = clf.predict(X_val)
    print("Validation Accuracy:", accuracy_score(y_val,pred_val))



    #camera_loop()
    cv2.destroyAllWindows()
            

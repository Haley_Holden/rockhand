
import cv2
import numpy as np
from skimage import measure
from sys import platform as sys_pf
import warnings


# Load in thumb image in color
sample = cv2.imread('thumb2.JPG', 1)

# Display original image
sample_small = cv2.resize(sample, (640, 480))
cv2.imshow('Original image',sample_small)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Create skin color mask 
sample = cv2.cvtColor(sample_small, cv2.COLOR_BGR2HSV)
lower_hsv = np.array([6,31,150])
upper_hsv = np.array([40,140,255])
mask = cv2.inRange(sample,lower_hsv,upper_hsv)


# Apply morphological operations on the image
kernel = np.ones((6, 6),np.uint8)
mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,iterations=8)

cv2.imshow("Mask", mask)
cv2.waitKey(0)
cv2.destroyAllWindows()


res2 = cv2.bitwise_and(sample_small, sample_small, mask = mask)

cv2.imshow("just a hand", res2)
cv2.waitKey(0)
cv2.destroyAllWindows()



ret,bbox = cv2.threshold(mask,127,255,cv2.THRESH_BINARY)

output = mask.copy()

im2,contours,hierarchy = cv2.findContours(bbox, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
finalMask = np.zeros([res2.shape[0], res2.shape[1]], dtype=np.int8) 

if len(contours) != 0:
    # the contours are drawn here
    output = cv2.drawContours(output, contours, -1, 255, 3)

    #find the biggest area of the contour
    c = max(contours, key = cv2.contourArea)
    
    x,y,w,h = cv2.boundingRect(c)
    
	# draw the 'human' contour (in green)
    output = cv2.rectangle(output,(x,y),(x+w,y+h),(255,255,255),-1)
	

    res2 = cv2.rectangle(res2,(x,y),(x+w,y+h),(50,140,45),1)

for i in range(w):
    for j in range(h):
        if x+i and y+j:
            finalMask[y+j][x+i] = 1

res2 = cv2.bitwise_and(res2, res2, mask = finalMask)


cv2.imshow("just a hand", res2)
cv2.waitKey(0)
cv2.destroyAllWindows()

# rockhand.py
import numpy as np
import cv2
import math


hand_cascade = cv2.CascadeClassifier('Hand_haar_cascade.xml')
imNum = 1832

while True:
    imName = "images/rock/IMG_" + str(imNum) + ".JPG"
    img = cv2.imread(imName, cv2.IMREAD_UNCHANGED)
    if img is not None:
        img = cv2.resize(img, (640, 480))
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE) 

	    # isolate hand from background
        blur = cv2.GaussianBlur(img,(5,5),0)
        greyscale = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
        retval2,mask = cv2.threshold(greyscale,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        mask = cv2.bitwise_not(mask)					

        hand = hand_cascade.detectMultiScale(mask) 
        mask2 = np.zeros(mask.shape, dtype = "uint8") 
        for (x,y,w,h) in hand: 
	        cv2.rectangle(img,(x,y),(x+w,y+h), (255,0,0), 2) 
	        cv2.rectangle(mask2, (x,y),(x+w,y+h),(255, 0, 0),-1)
        img2 = cv2.bitwise_and(mask, mask2)
        final = cv2.GaussianBlur(img2,(5,5),cv2.BORDER_DEFAULT)	
        _, contours, _ = cv2.findContours(final, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	
        cv2.drawContours(img, contours, 0, (0,255,0), 3)

        if len(contours) > 0:
            hnd = contours[0]
            hull = cv2.convexHull(hnd, returnPoints= False)
		
            defects = cv2.convexityDefects(hnd, hull)
            count = 0

            if defects is not None:

                for i in range(defects.shape[0]):
                    s, e, f, d = defects[i,0]
                    one = tuple(hnd[s][0])
                    two = tuple(hnd[e][0])
                    low = tuple(hnd[f][0])

                    a = math.sqrt(pow((two[0] - one[0]), 2) + pow((two[1] - one[1]), 2))
                    b = math.sqrt(pow((low[0] - one[0]), 2) + pow((low[1] - one[1]), 2))
                    c = math.sqrt(pow((two[0] - low[0]), 2) + pow((two[1] - low[1]), 2))

                    angle = math.acos((b*b + c*c - a*a)/(2*b*c)) * 57

                    if angle <= 90:
	                    count += 1

            
		


            if count == 1:
                cv2.putText(img, "peace", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
            elif count == 2:
                cv2.putText(img, "THIS IS 3", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0) , 2)
            elif count == 3:
                cv2.putText(img,"THIS is 4", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1,(255, 0, 0) , 2)
            elif count == 4:
                cv2.putText(img,"wave", (50, 50), cv2.FONT_HERSHEY_SIMPLEX,1,  (255, 0, 0) , 2)



	

        cv2.imshow(str(imNum),img)
        cv2.waitKey(0)
    imNum += 1


    cv2.destroyAllWindows()
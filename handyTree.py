#########################################################################
######## SOURCE CODE FOR SCIKIT-LEARN IMAGE RECOGNITION TUTORIAL ########
#########################################################################

import scipy.io 
import numpy as np
import matplotlib.pyplot as plt 

from sklearn.utils import shuffle 
from sklearn.metrics import accuracy_score 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.model_selection import train_test_split 
from sklearn import metrics


import h5py
import cv2

#########################################################################
# load our data file 
y_train_data = scipy.io.loadmat('yhandphotodata.mat')
#print(y_train_data.keys())
#print(type(y_train_data['y']))

f = h5py.File('Xhandphotodata.mat', 'r')
#print(type(f))

arrays = {}

for k,v in f.items():
    arrays[k] = np.array(v)


X = np.transpose(arrays['X'], (3, 2, 1, 0 ))
y_mid = np.transpose(y_train_data['y'], (1,0))

y_list = [ c[0] for c in y_mid ] 

y = np.array(y_list)


'''
# view an image (e.g. 25) and print its corresponding label
#img_index = 25
#plt.imshow((X[:,:,:,img_index] * 255).astype(np.uint8))
#plt.show()
#print(y[img_index])

#########################################################################

'''

# reshape our matrices into 1D vectors and shuffle (still maintaining the index pairings)
X = X.reshape(X.shape[0]*X.shape[1]*X.shape[2],X.shape[3]).T 
y = y.reshape(y.shape[0],) 
X, y = shuffle(X, y, random_state=42)  # use a seed of 42 to replicate the results of tutorial


# optional: reduce dataset to a selected size (rather than all 500k examples)
size = X.shape[0] 	# change to real number to reduce size
X = X[:size,:] 		# X.shape should be (num_examples,img_dimensions*colour_channels)
y = y[:size] 		# y.shape should be (num_examples,)


#########################################################################

# split data into training and testing sets 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=42) # 0.25 x 0.8 = 0.2


# define our classifier and print out specs
clf = RandomForestClassifier() 
print(clf) 

# fit the model on training data and predict on unseen test data
clf.fit(X_train, y_train) 
preds_test = clf.predict(X_test) 
preds_train = clf.predict(X_train)
preds_val = clf.predict(X_val)
# pred = x[:-1,:].reshape(1, -1) 	# if predicting a single example it needs reshaping

# check the accuracy of the predictive model

# testing score
test_score = metrics.f1_score(y_test, preds_test, labels=list(set(y_test)), average='micro')
print("Testing Accuracy:" , test_score)

#Training  score
train_score = metrics.f1_score(y_train, preds_train, labels=list(set(y_train)), average='micro')
print("Training Accuracy:" , train_score)


#Validation Score
val_score = metrics.f1_score(y_val, preds_val, labels=list(set(y_val)), average='micro')
print("Validation Accuracy:" , val_score)




#print("Accuracy:", accuracy_score(y_test,preds))



